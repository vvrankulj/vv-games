
#include "template.h"
#include "Globals.h"


using namespace AGK;
app App;

/////////////////////////////////////////
// PROTOTYPES ///////////////////////////
/////////////////////////////////////////

#ifdef IDE_ANDROID
extern void exit(int);
#endif

/////////////////////////////////////////
// CLASS STATIC ATTRIBUTES //////////////
/////////////////////////////////////////

bool app::did_start = false;
bool app::did_end = false;

/////////////////////////////////////////
// CLASS IMPLEMENTATION /////////////////
/////////////////////////////////////////
int sprite =0;
int spriteImage =0;

bool jumping = false;
bool falling = false;

int DoubleJump = 5;


// Begin app, called once at the start
void app::Begin( void )
{
	agk::SetVirtualResolution(640,480);

	GenerateEnvironment();
	AnimateCharacter();
	CreateCharacter();
	agk::CreateParticles ( 1, -320, -240 );
	agk::CreateText(5,"Congratulations!");

	//agk::GetSpriteInBox
}

void app::Loop ( void )
{
	int result = LastBlockDetected();

	if(result == 1){
		CreateMessageText();
		End();
	}else{
		UpdateEnvironment();
	}
	//agk::Print(result);


	app::did_start = true;
	int jump = agk::GetRawKeyPressed(38);
	agk::Print(jump);
	if(jump > 0){
		if(!jumping || !falling){
		StopAnimation();
		agk::SetSpritePhysicsVelocity(1,0,-400);
		jumping = true;
		falling = false;
		}
	}

	if(agk::Round(agk::GetSpritePhysicsVelocityY(1) > 0)){
		jumping = true;
		falling = false;
	}else if(agk::Round(agk::GetSpritePhysicsVelocityY(1) < 0)){
		jumping = false;
		falling = true;
	}else{
		jumping = false;
		falling = false;
	}
	if(!jumping && !falling){
		ResumeAnimation();
	}

	if(agk::GetSpriteY(1)>480){
		End();
	}

	// check for ended
	if (app::did_end) return;

	// check for closing
	if (agk::GetPointerPressed())
	{

	} else {

	}

	// sync display
	agk::Sync();
}

// Called when the app ends
void app::End ( void )
{
	DeleteEnvironment();
	DeleteCharacter();
	StartNewLevel();
}

#ifdef IDE_XCODE
// Called when the app returns from being in the background
void app::appGetResumed( void )
{
	// do anything that needs to be handled after being paused
	agk::Message("We have returned!");
}
#endif

void app::closeThisApp()
{
	// indicate done
	app::did_end = true;

	// completely exit the app
#ifdef AGKWINDOWS
	PostQuitMessage(0);
#endif
#ifdef AGKIOS
	// forcing a quit in iOS is against recommended guidelines - use HOME button
	// the exit button is disabled on AGKIOS builds
	// but if you want to do so, this is the code
	agk::MasterReset();
	exit(1);
#endif
#ifdef IDE_ANDROID
	// similar to iOS, an exit button should not be done
	// but if you want to do so, this is the code
	agk::MasterReset();
	exit(0);
#endif
#ifdef IDE_MAC
	glfwCloseWindow();
#endif
#ifdef IDE_MEEGO
	g_appptr->quit();
#endif
#ifdef IDE_BADA
	// Bada platform has a HOME button to quit apps
	// but the END command can also quit a Bada App forcefully
	Application::GetInstance()->Terminate();
#endif
}
