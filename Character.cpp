#include "Globals.h"
#include "template.h"


using namespace AGK;

int CharacterSprite;
int CharacterImage;
bool animationRolling = false;

void AnimateCharacter(){
	agk::CreateSprite ( 1, 0 );


	//agk::AddSpriteAnimationFrame ( 1, agk::LoadImage ( "..\\JumperImages\\JumperFrame1.png" ) );
	agk::AddSpriteAnimationFrame ( 1, agk::LoadImage ( "..\\JumperImages\\JumperFrame2.png" ) );
	agk::AddSpriteAnimationFrame ( 1, agk::LoadImage ( "..\\JumperImages\\JumperFrame3.png" ) );
	agk::AddSpriteAnimationFrame ( 1, agk::LoadImage ( "..\\JumperImages\\JumperFrame4.png" ) );

	
	agk::PlaySprite ( 1, 10, 1, 1, 5 );
	animationRolling = true;
}

void StopAnimation(){
	if(animationRolling){
		agk::StopSprite(1);
		animationRolling = false;
	}
}

void ResumeAnimation(){
	if(!animationRolling){
		agk::ResumeSprite(1);
		animationRolling = true;
	}
}

void CreateCharacter(){
	//CharacterImage = agk::LoadImage("..\\JumperMain.png");
	//CharacterSprite = agk::CreateSprite(CharacterImage);
	agk::SetSpritePositionByOffset(1,320, 50);
	agk::SetSpriteSize(1,25,-1);
	agk::SetPhysicsGravity(0,800);
	agk::SetSpritePhysicsOn(1,2);
	agk::SetSpritePhysicsRestitution(1,0);
	agk::SetSpritePhysicsTorque(1,0);
	agk::SetSpritePhysicsCanRotate(1,0);
}

void DeleteCharacter(){
	agk::DeleteSprite(1);
}


