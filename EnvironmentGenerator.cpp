
#include "Globals.h"
#include "template.h"

using namespace AGK;
using namespace std;

const int blockCnt = 35;
int environmentArray[blockCnt];
int spriteWidth = 0;
int currentEnvWidth = 0;
int g_currentSprite = 0;
int lastBlock = 0;
int fire = 1;


void GenerateEnvironment(){

	int screenWidth = 640;
	//int i = 0;

	int background = agk::CreateSprite(0);
	agk::SetSpriteSize(background,640,480);


	//Generate environment matrix
	char environmentMatrix[blockCnt];
	char lastChr = 'g';
	for(int j = 0; j<sizeof(environmentMatrix)/sizeof(char);j++){
		if(agk::Round(agk::Random(0,5))== 0 && lastChr != 'e'){
			environmentMatrix[j] = 'e';
			lastChr = 'e';
		}else if(j >= blockCnt - 5){
			environmentMatrix[j] = 'g';
			lastChr = 'g';
		}else if(j >= blockCnt -1){
			environmentMatrix[j] = '\0';
			lastChr = '\0';
		}else{
			environmentMatrix[j] = 'g';
			lastChr = 'g';
		}
	}

	for(int i = 0; i<blockCnt; i++){
		if(environmentMatrix[i] == 'g'){
			environmentArray[i] = agk::CreateSprite(agk::LoadImage("..\\JumperImages\\Building.png"));
			agk::SetSpritePosition(environmentArray[i],currentEnvWidth,agk::Random(300,400));
			currentEnvWidth += (int)agk::GetSpriteWidth(environmentArray[i]);

			agk::SetSpritePhysicsOn(environmentArray[i],3);
			agk::SetSpritePhysicsRestitution(environmentArray[i],0);
			agk::SetSpritePhysicsTorque(environmentArray[i],0);
		}else{
			currentEnvWidth += 100;
		}
	}
}

void DeleteEnvironment(){
	for(int i = 0; i<sizeof(environmentArray)/sizeof(int); i++){
		agk::DeleteSprite(environmentArray[i]);
	}
}

void UpdateEnvironment(){
	for(int j = 0; j < blockCnt; j++){
		if(agk::GetSpriteActive(environmentArray[j]) != 0){
			agk::SetSpritePhysicsOff(environmentArray[j]);
			agk::SetSpritePosition(environmentArray[j],agk::GetSpriteX(environmentArray[j])-2.1, agk::GetSpriteY(environmentArray[j]));
			agk::SetSpritePhysicsOn(environmentArray[j],1);

			agk::SetPhysicsWallBottom(0);
			agk::SetPhysicsWallLeft(1);
			agk::SetPhysicsWallRight(1);
			agk::SetPhysicsWallTop(1);

			if(j > 0){
				if(agk::GetSpriteX(environmentArray[j]) < -100){
					agk::DeleteSprite(environmentArray[j]);
				}
			}

		}else{
		}
	}
}

int LastBlockDetected(){
	//get level finished
	int endLevelSprite = agk::CreateSprite(0);
	int position = (blockCnt*-100)+640;
	agk::SetSpriteX(endLevelSprite,position);
	agk::SetSpriteY(endLevelSprite,400);
	if(agk::GetSpriteCollision(endLevelSprite, environmentArray[0])){
		return 1;
	}else{
		return 0;
	}
}

int CreateMessageText(){
	agk::SetTextPosition(5, 180,200);
	agk::SetTextSize(5, 26);
	agk::SetTextColor ( 5, agk::Random ( 1, 255 ), agk::Random ( 1, 255 ), 255 );

	if(fire == 1){
		agk::SetParticlesPosition ( 1, ( float ) agk::Random ( 100, 200 ), ( float ) agk::Random ( 100, 300 ) );
		agk::ResetParticleCount ( 1 );
		agk::SetParticlesFrequency ( 1, 250 );
		agk::SetParticlesLife ( 1, 3.0f );
		agk::SetParticlesSize ( 1, 64 );
		agk::SetParticlesStartZone( 1, -10, 0, 10, 0 );
		agk::SetParticlesImage ( 1, 1 );
		agk::SetParticlesDirection( 1, 10, 10.0f );
		agk::SetParticlesAngle ( 1, 360.0f );
		agk::SetParticlesVelocityRange ( 1, 0.8f, 2.5f );
		agk::SetParticlesMax ( 1, 500 );

		agk::AddParticlesColorKeyFrame ( 1, 0.0, 0, 0, 0, 0 );
		agk::AddParticlesColorKeyFrame ( 1, 0.5f, 255, 255, 0, 255 );
		agk::AddParticlesColorKeyFrame ( 1, 2.8f, 255, 0, 0, 0 );

		agk::AddParticlesForce ( 1, 2.0f, 2.8f, 25, -25 );
		fire = 0;
	}

	return 1;
}

int GetCollision(int sprite){
	for(int i = 0; i < environmentArray[sizeof(environmentArray)/sizeof(int)-1]; i++){
		if(agk::GetSpriteCollision(sprite,environmentArray[i])){
			return 1;
		}
		else
		{
			return 0;
		}
	}
}

