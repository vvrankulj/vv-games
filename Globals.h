#ifndef _GLOBALS_H_
#define _GLOBALS_H_

extern int g_currentSprite;
extern const int blockCnt;
extern int environmentArray[];

void CreateCharacter(void);
void DeleteCharacter(void);
void AnimateCharacter(void);
void StopAnimation(void);
void ResumeAnimation(void);

void GenerateEnvironment(void);
void DeleteEnvironment(void);
void UpdateEnvironment(void);
int LastBlockDetected(void);
int GetCollision(int);

void StartNewLevel(void);
void CreateNewLevel(void);


int CreateMessageText(void);

#endif

